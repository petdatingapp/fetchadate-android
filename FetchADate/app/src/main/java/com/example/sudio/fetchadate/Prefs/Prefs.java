package com.example.sudio.fetchadate.Prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class Prefs {
	
		
        private Context context = null;
        
        public static String KEY_FB_ACCESS_TOKEN="fb_access_token";
        
        public Prefs(Context context) {
                this.context = context;
        }

        @SuppressWarnings("unused")
		public String getString(String key, String def) {
                SharedPreferences prefs = PreferenceManager
                                .getDefaultSharedPreferences(context);
                String s = prefs.getString(key, def);
                return s;
        }

        @SuppressWarnings("unused")
		public int getInt(String key, int def) {
                SharedPreferences prefs = PreferenceManager
                                .getDefaultSharedPreferences(context);
                int i = Integer.parseInt(prefs.getString(key, Integer.toString(def)));
                return i;
        }

        @SuppressWarnings("unused")
		public float getFloat(String key, float def) {
                SharedPreferences prefs = PreferenceManager
                                .getDefaultSharedPreferences(context);
                float f = Float.parseFloat(prefs.getString(key, Float.toString(def)));
                return f;
        }

        public long getLong(String key, long def) {
                SharedPreferences prefs = PreferenceManager
                                .getDefaultSharedPreferences(context);
                long l = Long.parseLong(prefs.getString(key, Long.toString(def)));
                return l;
        }

		public void setString(String key, String val) {
                SharedPreferences prefs = PreferenceManager
                                .getDefaultSharedPreferences(context);
                Editor e = prefs.edit();
                e.putString(key, val);
                e.commit();
        }

		public void setBoolean(String key, boolean val) {
                SharedPreferences prefs = PreferenceManager
                                .getDefaultSharedPreferences(context);
                Editor e = prefs.edit();
                e.putBoolean(key, val);
                e.commit();
        }

        @SuppressWarnings("unused")
		public void setInt(String key, int val) {
                SharedPreferences prefs = PreferenceManager
                                .getDefaultSharedPreferences(context);
                Editor e = prefs.edit();
                e.putString(key, Integer.toString(val));
                e.commit();
        }

		public void setLong(String key, long val) {
                SharedPreferences prefs = PreferenceManager
                                .getDefaultSharedPreferences(context);
                Editor e = prefs.edit();
                e.putString(key, Long.toString(val));
                e.commit();
        }

		public boolean getBoolean(String key, boolean def) {
                SharedPreferences prefs = PreferenceManager
                                .getDefaultSharedPreferences(context);
                boolean b = prefs.getBoolean(key, def);
                return b;
        }

        
        public String getFbAccessToken(String key,String val)
        {
        	SharedPreferences prefs = PreferenceManager
            .getDefaultSharedPreferences(context);
        	String str=prefs.getString(key, val);
        	return str;
        }
        
        public void clearFBToken() {
        	setFbAccessToken(KEY_FB_ACCESS_TOKEN,"");
        }
        
    	public void setFbAccessToken(String key, String val)
      {
    	  setString(key, val);
      }
       
        public void setLongValue(String key,long val)
       {
    	   setLong(key, val);
       }
       
        public long getLongValue(String key)
       {
    	  return getLong(key, 0);
       }
       
       //   Twitter Preferences function
       
       public void setTWTProfileImage(String profileImage) {
      	setString("TWTprofilePic", profileImage);
    	}

    	public String getTWTProfileImage() {
      		return getString("TWTprofilePic", "");
      		
    	}
      	
    public void setTwID(String TwID) {
   		setString("TwID", TwID);
       	}

   	public String getTwID() {
   		return getString("TwID", "");
   	}
   	
   	public void setTwName(String TwName) {
   		setString("TwName", TwName);
   	}

   	public String getTwName() {
   		return getString("TwName", "link >");
   	}
   	
   	public void setTwitterStatus(boolean TwitterStatus) {
   		setBoolean("TwitterStatus", TwitterStatus);
   		if(TwitterStatus)
   			setBoolean("OnlineStatus", TwitterStatus);
   	}

   	public boolean getTwitterStatus() {
   		return getBoolean("TwitterStatus", false);
   		
   	}

   	public void setTwitterTokenSecret(String tokenSecret) {
   		setString("TwTTokenSecret", tokenSecret);
   	}
   	
   	public String getTwitterTokenSecret() {
   		 
   		return getString("TwTTokenSecret", "");
   	}

   	public void setTwitterAccessToken(String accessToken) {
   		setString("TwTAccessToken", accessToken);
   		
   	}
   	
   	public String getTwitterAccessToken(){
   		return getString("TwTAccessToken", "");
   	}


   	//  Linkedin Preferences function
   	
	public void setLinkedinTokenSecret(String tokenSecret) {
   		setString("LNDTokenSecret", tokenSecret);
   	}
   	
   	public String getLinkedinTokenSecret() {
   		 
   		return getString("LNDTokenSecret", "");
   	}

   	public void setLinkedinAccessToken(String accessToken) {
   		setString("LNDAccessToken", accessToken);
   		
   	}
   	
   	public String getLinkedinAccessToken(){
   		return getString("LNDAccessToken", "");
   	}
   	
	public void setLinkedinVerifier(String Verifier) {
   		setString("LNDVerifier", Verifier);
   		
   	}
   	
   	public String getLinkedinVerifier(){
   		return getString("LNDVerifier", "");
   	}
   	
   	public void setExpiredTime(long time) {
   		setLong("ExpiredTime", time);
   		
   	}
   	
   	public long getExpiredTime(){
   		return getLong("ExpiredTime", 0);
   	}
   	
   	public void setLinkedInStatus(boolean LinkedInStatus) {
   		setBoolean("LinkedInStatus", LinkedInStatus);
   		if(LinkedInStatus)
   			setBoolean("OnlineStatus", LinkedInStatus);
   	}

   	public boolean getLinkedInStatus() {
   		return getBoolean("LinkedInStatus", false);
   		
   	}
	
   //   Gplus Preferences function
   	
   	public void setGplusAccessToken(String accessToken) {
   		setString("GplusAccessToken", accessToken);
   		
   	}
   	
   	public String getGplusAccessToken(){
   		return getString("GplusAccessToken", "");
   	}
   	
	public void setGplusExpiredTime(Long accessToken) {
   		setLong("GplusExpiredTime", accessToken);
   		
   	}
   	
   	public Long getGplusExpiredTime(){
   		return getLong("GplusExpiredTime", 0);
   	}
   
   	public void setGplusRefreshToken(String accessToken) {
   		setString("GplusRefreshToken", accessToken);
   		
   	}
   	
   	public String getGplusRefreshToken(){
   		return getString("GplusRefreshToken", "");
   	}
   	
	public void setGplusScope(String accessToken) {
   		setString("GplusScope", accessToken);
   		
   	}
   	
   	public String getGplusScope(){
   		return getString("GplusScope", "");
   	}
   	
	public void setGplusStatus(boolean GplusStatus) {
   		setBoolean("GplusStatus", GplusStatus);
   	}

   	public boolean getGplusStatus() {
   		return getBoolean("GplusStatus", false);
   		
   	}
   
}
