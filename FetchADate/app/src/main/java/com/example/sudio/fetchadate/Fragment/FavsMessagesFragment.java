package com.example.sudio.fetchadate.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sudio.fetchadate.Activity.MainActivity;
import com.example.sudio.fetchadate.Adapter.MyFavsMsgItemsAdapter;
import com.example.sudio.fetchadate.R;

import java.util.ArrayList;

public class FavsMessagesFragment extends Fragment implements View.OnClickListener, MyFavsMsgItemsAdapter.MyFasMsgItemClickListener{

    private View rootView;
    private TextView tvMessages, tvMyFavs, tvFavdMe, tvAll;
    private RelativeLayout rlFavMsgMenuAll;
    private Button btnFavMsgOpenChat, btnFavMsgOpenPrf, btnFavMsgRecFrds, btnFavMsgFrdReport, btnFavMsgUnMatch, btnFavMsgCancel;

    private RecyclerView myFavMsg_RecView;
    private MyFavsMsgItemsAdapter myFavMsg_Adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_fav_messages, container, false);

        init();
        click();
        setMyFavMsgItemAdapter();
        return rootView;
    }

    private void init(){
        rlFavMsgMenuAll = rootView.findViewById(R.id.rlFavMsgMenuAll);
        rlFavMsgMenuAll.setVisibility(View.INVISIBLE);

        tvMessages = rootView.findViewById(R.id.tvMessages);
        tvMyFavs = rootView.findViewById(R.id.tvMyFavs);
        tvFavdMe = rootView.findViewById(R.id.tvFavdMe);
        tvAll = rootView.findViewById(R.id.tvAll);

        btnFavMsgOpenChat = rootView.findViewById(R.id.btnFavMsgOpenChat);
        btnFavMsgOpenPrf = rootView.findViewById(R.id.btnFavMsgOpenPrf);
        btnFavMsgRecFrds = rootView.findViewById(R.id.btnFavMsgRecFrds);
        btnFavMsgFrdReport = rootView.findViewById(R.id.btnFavMsgFrdReport);
        btnFavMsgUnMatch = rootView.findViewById(R.id.btnFavMsgUnMatch);
        btnFavMsgCancel = rootView.findViewById(R.id.btnFavMsgCancel);
    }

    private void click(){
        tvMessages.setOnClickListener(FavsMessagesFragment.this);
        tvMyFavs.setOnClickListener(FavsMessagesFragment.this);
        tvFavdMe.setOnClickListener(FavsMessagesFragment.this);
        tvAll.setOnClickListener(FavsMessagesFragment.this);

        btnFavMsgOpenChat.setOnClickListener(FavsMessagesFragment.this);
        btnFavMsgOpenPrf.setOnClickListener(FavsMessagesFragment.this);
        btnFavMsgRecFrds.setOnClickListener(FavsMessagesFragment.this);
        btnFavMsgFrdReport.setOnClickListener(FavsMessagesFragment.this);
        btnFavMsgUnMatch.setOnClickListener(FavsMessagesFragment.this);
        btnFavMsgCancel.setOnClickListener(FavsMessagesFragment.this);
    }

    private void setMyFavMsgItemAdapter() {
        // data to populate the RecyclerView with
        ArrayList<String> animalNames = new ArrayList<>();
        animalNames.add("Larry, 378");
        animalNames.add("Molly, 373");
        animalNames.add("Jolly, 375");
        animalNames.add("Polly, 379");

        ArrayList<String> dogLoversNames = new ArrayList<>();
        dogLoversNames.add("Jorden, 26");
        dogLoversNames.add("Jessica, 23");
        dogLoversNames.add("Horden, 25");
        dogLoversNames.add("Messica, 29");

        ArrayList<Integer> myAminalImageList = new ArrayList<>();
        ArrayList<Integer> myAminalLoverList = new ArrayList<>();
        myAminalImageList.add(R.drawable.wingdog_photo);
        myAminalImageList.add(R.drawable.dogdob_bg);
        myAminalImageList.add(R.drawable.wingdog_photo);
        myAminalImageList.add(R.drawable.dogdob_bg);

        myAminalLoverList.add(R.drawable.boy);
        myAminalLoverList.add(R.drawable.girl);
        myAminalLoverList.add(R.drawable.boy);
        myAminalLoverList.add(R.drawable.girl);

        // set up the RecyclerView
        myFavMsg_RecView = rootView.findViewById(R.id.myFavMsg_RecView);
        myFavMsg_RecView.setLayoutManager(new LinearLayoutManager(getActivity()));
        myFavMsg_Adapter = new MyFavsMsgItemsAdapter(getActivity(), animalNames,dogLoversNames,myAminalImageList,myAminalLoverList);
        myFavMsg_Adapter.setClickListener(this);
        myFavMsg_RecView.setAdapter(myFavMsg_Adapter);

        myFavMsg_RecView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(rlFavMsgMenuAll.getVisibility() == View.VISIBLE)
                    animateDown(rlFavMsgMenuAll);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == tvMessages) {
            ((MainActivity)getActivity()).loadFavsMessagesFragment();
        } else if (v == tvMyFavs) {
            ((MainActivity)getActivity()).loadMyFavsFragment();
        } else if (v == tvFavdMe) {
            ((MainActivity)getActivity()).loadFavdMeFragment();
        } else if (v == tvAll) {

        } else if (v == btnFavMsgCancel) {
            animateDown(rlFavMsgMenuAll);
        }
    }

    private void animateDown(final RelativeLayout layout) {
        try {
            btnFavMsgOpenChat.setEnabled(false);
            btnFavMsgOpenPrf.setEnabled(false);
            btnFavMsgRecFrds.setEnabled(false);
            btnFavMsgFrdReport.setEnabled(false);
            btnFavMsgUnMatch.setEnabled(false);
            btnFavMsgCancel.setEnabled(false);

            Animation animator = AnimationUtils.loadAnimation((MainActivity)getActivity(), R.anim.pop_down);
            LayoutAnimationController animController=null;
            animController=new LayoutAnimationController(animator);
            layout.setLayoutAnimation(animController);
            layout.startAnimation(animator);
            layout.setVisibility(View.INVISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void animateShow(final RelativeLayout layout) {
        try {
            btnFavMsgOpenChat.setEnabled(true);
            btnFavMsgOpenPrf.setEnabled(true);
            btnFavMsgRecFrds.setEnabled(true);
            btnFavMsgFrdReport.setEnabled(true);
            btnFavMsgUnMatch.setEnabled(true);
            btnFavMsgCancel.setEnabled(true);

            Animation animator = AnimationUtils.loadAnimation((MainActivity)getActivity(), R.anim.popup_show);
            LayoutAnimationController animController=null;
            animController=new LayoutAnimationController(animator);
            layout.setLayoutAnimation(animController);
            layout.startAnimation(animator);

            animator.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    layout.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showMyFavMsgMenu() {
        try{
            if(rlFavMsgMenuAll.getVisibility() == View.INVISIBLE)
                animateShow(rlFavMsgMenuAll);
            else
                animateDown(rlFavMsgMenuAll);
        } catch (Exception e){
            e.getMessage();
        }
    }

    @Override
    public void onMyFavMsgItemClick(View view, int position) {
        switch (view.getId()){
            case R.id.itemFavsMsg_Menu:
                showMyFavMsgMenu();
                //Toast.makeText(getActivity(), "You clicked " + myFavMsg_Adapter.getItem(position) + " on row number " + (position+1), Toast.LENGTH_SHORT).show();
                break;
        }
    }
}