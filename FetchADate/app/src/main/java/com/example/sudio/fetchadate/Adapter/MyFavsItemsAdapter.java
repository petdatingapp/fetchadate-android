package com.example.sudio.fetchadate.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sudio.fetchadate.R;

import java.util.ArrayList;
import java.util.List;

public class MyFavsItemsAdapter extends RecyclerView.Adapter<MyFavsItemsAdapter.ViewHolder> {

    private List<String> mDataDogNames;
    private List<String> mDataLoversNames;

    private ArrayList<Integer> mAminalImg ;
    private ArrayList<Integer> mAminalLovers ;

    private LayoutInflater mInflater;
    private ItemClickListener mMyFavClickListener;
    private Context mContext;

    // dataDogs is passed into the constructor
    public MyFavsItemsAdapter(Context context, List<String> dataDogs, List<String> dataLovers, ArrayList<Integer> AminalImages, ArrayList<Integer> AminalLovers) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mDataDogNames = dataDogs;
        this.mDataLoversNames = dataLovers;
        this.mAminalImg = AminalImages;
        this.mAminalLovers = AminalLovers;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_my_favs, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String dogName = mDataDogNames.get(position);
        String dogLoversName = mDataLoversNames.get(position);
        holder.itemMyFav_txtDogName.setText(dogName);
        holder.itemMyFav_txtDogLoverName.setText(dogLoversName);

        holder.itemMyFav_ImgDog.setImageResource(mAminalImg.get(position));
        holder.itemMyFav_ImgDogLover.setImageResource(mAminalLovers.get(position));
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mDataDogNames.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout itemMyFav_LinMain;
        ImageView itemMyFav_ImgDog,itemMyFav_ImgDogLover,itemMyFav_menu;
        TextView itemMyFav_txtDogName,itemMyFav_txtDogLoverName;

        ViewHolder(View itemView) {
            super(itemView);
            itemMyFav_LinMain = (LinearLayout) itemView.findViewById(R.id.itemMyFav_LinMain);
            itemMyFav_ImgDog = (ImageView) itemView.findViewById(R.id.itemMyFav_ImgDog);
            itemMyFav_ImgDogLover = (ImageView) itemView.findViewById(R.id.itemMyFav_ImgDogLover);
            itemMyFav_menu = (ImageView) itemView.findViewById(R.id.itemMyFav_menu);
            itemMyFav_txtDogName = (TextView) itemView.findViewById(R.id.itemMyFav_txtDogName);
            itemMyFav_txtDogLoverName = (TextView) itemView.findViewById(R.id.itemMyFav_txtDogLoverName);

            itemMyFav_menu.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mMyFavClickListener != null) mMyFavClickListener.onMyFavItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return mDataDogNames.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mMyFavClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onMyFavItemClick(View view, int position);
    }
}
