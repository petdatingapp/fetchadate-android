package com.example.sudio.fetchadate.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sudio.fetchadate.Activity.MainActivity;
import com.example.sudio.fetchadate.Adapter.MyFavsItemsAdapter;
import com.example.sudio.fetchadate.R;

import java.util.ArrayList;

public class MyFavsFragment extends Fragment implements View.OnClickListener, MyFavsItemsAdapter.ItemClickListener {

    private View rootView;
    private RelativeLayout rlFavMenu;
    private TextView tvMessages, tvMyFavs, tvFavdMe, tvAll;
    private Button btnFavOpenPrf, btnFavRecFrds, btnFavFrdDelete,btnFavCancel;

    private MyFavInterface myFavlistener ;

    private RecyclerView myFav_RecView;
    private MyFavsItemsAdapter myFav_Adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_favs, container, false);

        init();
        click();
        setMyFavItemAdapter();

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            myFavlistener = (MyFavInterface) getActivity();
        } catch (ClassCastException castException) {
            castException.getMessage();
        }
    }

    private void init(){
        rlFavMenu = rootView.findViewById(R.id.rlFavMenu);
        rlFavMenu.setVisibility(View.INVISIBLE);

        tvMessages = rootView.findViewById(R.id.tvMessages);
        tvMyFavs = rootView.findViewById(R.id.tvMyFavs);
        tvFavdMe = rootView.findViewById(R.id.tvFavdMe);
        tvAll = rootView.findViewById(R.id.tvAll);

        btnFavOpenPrf = rootView.findViewById(R.id.btnFavOpenPrf);
        btnFavRecFrds = rootView.findViewById(R.id.btnFavRecFrds);
        btnFavFrdDelete = rootView.findViewById(R.id.btnFavFrdDelete);
        btnFavCancel = rootView.findViewById(R.id.btnFavCancel);
    }

    private void click(){
        tvMessages.setOnClickListener(MyFavsFragment.this);
        tvMyFavs.setOnClickListener(MyFavsFragment.this);
        tvFavdMe.setOnClickListener(MyFavsFragment.this);
        tvAll.setOnClickListener(MyFavsFragment.this);
        btnFavCancel.setOnClickListener(MyFavsFragment.this);
    }

    private void setMyFavItemAdapter() {

        // data to populate the RecyclerView with
        ArrayList<String> animalNames = new ArrayList<>();
        animalNames.add("Larry, 378");
        animalNames.add("Molly, 373");
        animalNames.add("Jolly, 375");
        animalNames.add("Polly, 379");

        ArrayList<String> dogLoversNames = new ArrayList<>();
        dogLoversNames.add("Jorden, 26");
        dogLoversNames.add("Jessica, 23");
        dogLoversNames.add("Horden, 25");
        dogLoversNames.add("Messica, 29");

        ArrayList<Integer> myAminalImageList = new ArrayList<>();
        ArrayList<Integer> myAminalLoverList = new ArrayList<>();
        myAminalImageList.add(R.drawable.wingdog_photo);
        myAminalImageList.add(R.drawable.dogdob_bg);
        myAminalImageList.add(R.drawable.wingdog_photo);
        myAminalImageList.add(R.drawable.dogdob_bg);

        myAminalLoverList.add(R.drawable.boy);
        myAminalLoverList.add(R.drawable.girl);
        myAminalLoverList.add(R.drawable.boy);
        myAminalLoverList.add(R.drawable.girl);

        // set up the RecyclerView
        myFav_RecView = rootView.findViewById(R.id.myFav_RecView);
        myFav_RecView.setLayoutManager(new LinearLayoutManager(getActivity()));
        myFav_Adapter = new MyFavsItemsAdapter(getActivity(), animalNames,dogLoversNames,myAminalImageList,myAminalLoverList);
        myFav_Adapter.setClickListener(this);
        myFav_RecView.setAdapter(myFav_Adapter);

        myFav_RecView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(rlFavMenu.getVisibility()==View.VISIBLE)
                    animateDown(rlFavMenu);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == tvMessages) {
            ((MainActivity)getActivity()).loadFavsMessagesFragment();
        } else if (v == tvMyFavs) {
            ((MainActivity)getActivity()).loadMyFavsFragment();
        } else if (v == tvFavdMe) {
            ((MainActivity)getActivity()).loadFavdMeFragment();
        } else if (v == tvAll) {

            try{
                myFavlistener.myFavAction();
            }catch (Exception ex){
            ex.getMessage();
            }
        }
        else if (v == btnFavCancel) {
            animateDown(rlFavMenu);
        }
    }

    @Override
    public void onMyFavItemClick(View view, int position) {
        switch (view.getId()){
            case R.id.itemMyFav_menu:
                showMyFavMenu();
                //Toast.makeText(getActivity(), "You clicked " + myFav_Adapter.getItem(position) + " on row number " + (position+1), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void animateDown(final RelativeLayout layout) {
        try {
            btnFavOpenPrf.setEnabled(false);
            btnFavRecFrds.setEnabled(false);
            btnFavFrdDelete.setEnabled(false);
            btnFavCancel.setEnabled(false);

            Animation animator = AnimationUtils.loadAnimation((MainActivity) getActivity(), R.anim.pop_down);
            LayoutAnimationController animController = null;
            animController = new LayoutAnimationController(animator);
            layout.setLayoutAnimation(animController);
            layout.startAnimation(animator);
            layout.setVisibility(View.INVISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void animateShow(final RelativeLayout layout) {
        try {
            btnFavOpenPrf.setEnabled(true);
            btnFavRecFrds.setEnabled(true);
            btnFavFrdDelete.setEnabled(true);
            btnFavCancel.setEnabled(true);

            Animation animator = AnimationUtils.loadAnimation((MainActivity)getActivity(), R.anim.popup_show);
            LayoutAnimationController animController=null;
            animController = new LayoutAnimationController(animator);
            layout.setLayoutAnimation(animController);
            layout.startAnimation(animator);

            animator.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    layout.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showMyFavMenu() {
        try{

            if(rlFavMenu.getVisibility() == View.INVISIBLE)
                animateShow(rlFavMenu);
            else
                animateDown(rlFavMenu);

        }catch (Exception e){
            e.getMessage();
        }
    }

    public interface MyFavInterface {
        void myFavAction() ;
    }

}