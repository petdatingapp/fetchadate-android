package com.example.sudio.fetchadate.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.sudio.fetchadate.R;

public class GuyGalActivity extends AppCompatActivity implements View.OnClickListener{

    ImageButton imgBtnBack;
    ProgressBar iProgrssBar;
    Button Gender_btnGuy, Gender_btnGal, btnNext;
    int GuyGal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guygal);

        iProgrssBar = (ProgressBar)findViewById(R.id.progressBar_DogLover);
        iProgrssBar.setProgress(20);

        imgBtnBack = (ImageButton)findViewById(R.id.ImgButtonBack_DogLover);
        Gender_btnGuy = (Button)findViewById(R.id.Gender_btnGuy);
        Gender_btnGal = (Button)findViewById(R.id.Gender_btnGal);
        btnNext = (Button)findViewById(R.id.Gender_btnNext);

        imgBtnBack.setOnClickListener(this);
        Gender_btnGuy.setOnClickListener(this);
        Gender_btnGal.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        GuyGal=0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(GuyGal>0) {
            setValueGuyGal();
        }
    }
    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            //handle multiple view click events
            case R.id.ImgButtonBack_DogLover:
                Intent iSigninInt = new Intent(GuyGalActivity.this,SignInActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(iSigninInt);
                break;

            case R.id.Gender_btnGuy:
                GuyGal=1;
                setValueGuyGal();
                break;

            case R.id.Gender_btnGal:
                GuyGal=2;
                setValueGuyGal();
                break;

            case R.id.Gender_btnNext:
                if(GuyGal == 0){
                    Toast.makeText(getApplicationContext(), R.string.Gender_ValidationMessage, Toast.LENGTH_LONG).show();
                }else {
                    Intent iWithADogint = new Intent(GuyGalActivity.this, WithADogActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(iWithADogint);
                }
                break;
        }
    }

    public void setValueGuyGal(){
        if(GuyGal==1){
            ((Button) findViewById(R.id.Gender_btnGuy)).setTextColor(getResources().getColor(R.color.colorAccent));
            ((Button) findViewById(R.id.Gender_btnGuy)).setBackgroundResource(R.drawable.mycusto_btn2);
            ((Button) findViewById(R.id.Gender_btnGal)).setTextColor(getResources().getColor(R.color.colorBlack));
            ((Button) findViewById(R.id.Gender_btnGal)).setBackgroundResource(R.drawable.mycusto_btn1);
            ((Button) findViewById(R.id.Gender_btnNext)).setBackground(getResources().getDrawable(R.drawable.button_normal));
        }
        else if(GuyGal==2){
            ((Button) findViewById(R.id.Gender_btnGal)).setTextColor(getResources().getColor(R.color.colorAccent));
            ((Button) findViewById(R.id.Gender_btnGal)).setBackgroundResource(R.drawable.mycusto_btn2);
            ((Button) findViewById(R.id.Gender_btnGuy)).setTextColor(getResources().getColor(R.color.colorBlack));
            ((Button) findViewById(R.id.Gender_btnGuy)).setBackgroundResource(R.drawable.mycusto_btn1);
            ((Button) findViewById(R.id.Gender_btnNext)).setBackground(getResources().getDrawable(R.drawable.button_normal));
        }
    }
}
