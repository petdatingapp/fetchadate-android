package com.example.sudio.fetchadate.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.example.sudio.fetchadate.Prefs.AppData;
import com.example.sudio.fetchadate.Prefs.Prefs;
import com.example.sudio.fetchadate.R;

public class DogNameActivity extends AppCompatActivity  implements View.OnClickListener{

    ImageButton imgBtnBack;
    ProgressBar iProgrssBar;
    Button btnNext;
    EditText edtDogName;
    Prefs prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dog_name);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        imgBtnBack = (ImageButton)findViewById(R.id.ImgButtonBack_DogLover);
        imgBtnBack.setOnClickListener(this);
        iProgrssBar = (ProgressBar)findViewById(R.id.progressBar_DogLover);
        iProgrssBar.setProgress(60);

        btnNext = (Button)findViewById(R.id.DogName_btnNext);
        btnNext.setOnClickListener(this);

        edtDogName = (EditText) findViewById(R.id.DogName_EditxtDog);
        edtDogName.requestFocus();

        edtDogName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                View DV = (View)findViewById(R.id.DogName_DividerView);
                try {
                    if (editable.toString().length()>0) {
                        DV.setBackgroundResource(R.color.colorAccent);
                      }
                    else if (editable.toString().length()==0) {
                        DV.setBackgroundResource(R.color.colorLightGrey);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        edtDogName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    AppData.hideKeyboard(DogNameActivity.this,v);
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        getName();
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            //handle multiple view click events
            case R.id.ImgButtonBack_DogLover:
                Intent iIntent = new Intent(DogNameActivity.this,WithADogActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(iIntent);
                break;

            case R.id.DogName_btnNext:

                if(edtDogName.getText().toString().equals("")){
                    edtDogName.setError(this.getString(R.string.DogName_ValidationMessage));
                }
                else {
               //   Intent iDogDoint = new Intent(DogNameActivity.this, GreatTimeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Intent iDogDoint = new Intent(DogNameActivity.this, DogDobActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               //   iDogDoint.putExtra("Name", edtDogName.getText().toString());
                    setName();
                    startActivity(iDogDoint);
                    break;
                }

        }
    }

    public void setName(){
        prefs=new Prefs(DogNameActivity.this);
        prefs.setString("Name", edtDogName.getText().toString());
    }

    public void getName(){
        prefs=new Prefs(DogNameActivity.this);
        String name= prefs.getString("Name", "");

        if(name.equalsIgnoreCase("")){
            edtDogName.setText(name);
        }
    }


}
