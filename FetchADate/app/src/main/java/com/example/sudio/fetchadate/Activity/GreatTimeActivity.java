package com.example.sudio.fetchadate.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.sudio.fetchadate.Prefs.Prefs;
import com.example.sudio.fetchadate.R;

public class GreatTimeActivity extends AppCompatActivity {

    TextView txtGreatTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.great_time);

        setTextName();


    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
        delayActivity();
    }

    private void setTextName()
    {
        txtGreatTime = (TextView)findViewById(R.id.txtGreatTime);
        Prefs prefs=new Prefs(GreatTimeActivity.this);
        String message = prefs.getString("Name","");
//        Bundle bundle = getIntent().getExtras();
//        String message = bundle.getString("Name");
        txtGreatTime.setText(this.getString(R.string.txt_greattime));
        txtGreatTime.append(message);
    }
    private void delayActivity()
    {
        try {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
            //      Intent i =new Intent(GreatTimeActivity.this,DogDobActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Intent i =new Intent(GreatTimeActivity.this,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
                    finish();
                }
            }, 2000);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
