package com.example.sudio.fetchadate.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sudio.fetchadate.Activity.MainActivity;
import com.example.sudio.fetchadate.R;

public class FavdMeFragment extends Fragment implements View.OnClickListener{

    private View rootView;
    private TextView tvMessages, tvMyFavs, tvFavdMe, tvAll;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_favd_me, container, false);

        init();
        click();

        return rootView;
    }

    private void init(){
        tvMessages = rootView.findViewById(R.id.tvMessages);
        tvMyFavs = rootView.findViewById(R.id.tvMyFavs);
        tvFavdMe = rootView.findViewById(R.id.tvFavdMe);
        tvAll = rootView.findViewById(R.id.tvAll);
    }

    private void click(){
        tvMessages.setOnClickListener(FavdMeFragment.this);
        tvMyFavs.setOnClickListener(FavdMeFragment.this);
        tvFavdMe.setOnClickListener(FavdMeFragment.this);
        tvAll.setOnClickListener(FavdMeFragment.this);
    }

    @Override
    public void onClick(View v) {
        if (v == tvMessages) {
            ((MainActivity)getActivity()).loadFavsMessagesFragment();
        } else if (v == tvMyFavs) {
            ((MainActivity)getActivity()).loadMyFavsFragment();
        } else if (v == tvFavdMe) {
            ((MainActivity)getActivity()).loadFavdMeFragment();
        } else if (v == tvAll) {

        }
    }
}
