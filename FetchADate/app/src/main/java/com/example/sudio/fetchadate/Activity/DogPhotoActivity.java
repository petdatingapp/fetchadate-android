package com.example.sudio.fetchadate.Activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sudio.fetchadate.Prefs.AppData;
import com.example.sudio.fetchadate.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DogPhotoActivity extends AppCompatActivity implements View.OnClickListener{

    ImageButton imgBtnBack;
    ProgressBar iProgrssBar;
    Button btnPlus, btnAddPhoto;
    TextView imageDetails = null;
    ImageView showImg = null;

    Uri imageUri = null;
    Intent ImageIntent = null;
    Bitmap showImgBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dog_photo);

        imgBtnBack = (ImageButton)findViewById(R.id.ImgButtonBack_DogLover);
        imgBtnBack.setOnClickListener(this);
        iProgrssBar = (ProgressBar)findViewById(R.id.progressBar_DogLover);
        iProgrssBar.setProgress(100);
        showImg = (ImageView)findViewById(R.id.Windog_ImgPhoto);
        imageDetails = (TextView)findViewById(R.id.Windog_TxtTakeUpload);

        btnPlus = (Button)findViewById(R.id.Windog_btnPlus);
        btnPlus.setOnClickListener(this);

        btnAddPhoto = (Button)findViewById(R.id.Windog_btnAddPhoto);
        btnAddPhoto.setOnClickListener(this);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            btnAddPhoto.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE }, 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BitmapDrawable bitmapDrawable = ((BitmapDrawable) showImg.getDrawable());

        if(bitmapDrawable==null){
            showImg.buildDrawingCache();
            showImgBitmap = showImg.getDrawingCache();
            showImg.buildDrawingCache(false);
        } else {
            showImgBitmap = bitmapDrawable.getBitmap();
        }

        showImg.setImageBitmap(showImgBitmap);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                btnAddPhoto.setEnabled(true);
            }
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //handle multiple view click events
            case R.id.ImgButtonBack_DogLover:
                Intent iIntent = new Intent(DogPhotoActivity.this,DogDobActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(iIntent);
                break;

            case R.id.Windog_btnPlus:
                selectImage();
                break;
            case R.id.Windog_btnAddPhoto:
                if (ImageIntent == null || showImgBitmap == null) {
                    selectImage();
                } else {
                    Intent iIntent1 = new Intent(DogPhotoActivity.this, GreatTimeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(iIntent1);
                }
                break;

        }
    }


    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(DogPhotoActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = AppData.checkPermission(DogPhotoActivity.this);
                if (items[item].equals("Take Photo")) {
                    if(result)
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    if(result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent(){
        ImageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE );

        File photoFile = null;
        try {
            photoFile = createImageFile();
            Log.v("aa", photoFile.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                imageUri = FileProvider.getUriForFile(DogPhotoActivity.this,
                        getApplicationContext().getPackageName() + ".provider", photoFile);
                ImageIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                ImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(ImageIntent, 1);
            } else {
                imageUri = Uri.fromFile(photoFile);
                ImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(ImageIntent, 1);
            }
        }
    }

    private void galleryIntent(){
        ImageIntent = new Intent(Intent.ACTION_PICK);
        ImageIntent.setType("image/*");
        startActivityForResult(ImageIntent, 2);
    }

    private File createImageFile() throws IOException{
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMG_" + timeStamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir     /* directory */
        );
        return image;
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == 1 && resultCode == RESULT_OK) {  /*********** Load Image from camera ****************/
                performCrop(imageUri);
            } else if (requestCode == 2 && resultCode == RESULT_OK && data != null) {  /*********** Load Image from gallery ****************/
                imageUri = data.getData();
                performCrop(imageUri);
            } else if (requestCode == 3) {  /*********** Load Image after crop ****************/
                Bitmap bmImg = null;
                try{
                    bmImg = data.getExtras().getParcelable("data");
                } catch (Exception e) {
                    String filepath;
                    Cursor cursor = getContentResolver().query(imageUri, null, null, null, null);
                    if (cursor == null) {
                        filepath = imageUri.getPath();
                    } else {
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                        filepath = cursor.getString(columnIndex);
                        cursor.close();
                    }
                    bmImg = BitmapFactory.decodeFile(filepath);
                }
                Bitmap bmCompress = Bitmap.createScaledBitmap(bmImg, showImg.getWidth(), showImg.getHeight(), true);
                showImg.setImageBitmap(bmCompress);
                showImg.setScaleType(ImageView.ScaleType.FIT_XY);

                btnAddPhoto.setBackground(getResources().getDrawable(R.drawable.button_normal));
                btnAddPhoto.setText(getResources().getText(R.string.NextTxt));
            } else {
                Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void performCrop(Uri contentUri) {
        try {
            //Start Crop Activity
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            /*File f = new File(picUri);
            Uri contentUri = Uri.fromFile(f);*/

            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 200);
            cropIntent.putExtra("outputY", 200);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, 3);
        }
        // respond to users whose devices do not support the crop action
        catch (Exception e) {
            // display an error message
            e.printStackTrace();
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /*public static Bitmap showBitmapFromFile(String file_path) {
        try {
            File imgFile = new  File(file_path);
            if(imgFile.exists()){
                Bitmap pic_Bitmap = decodeFile(file_path);

                ExifInterface ei = new ExifInterface(file_path);
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                Bitmap rotatedBitmap = null;
                switch(orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotatedBitmap = rotateImage(pic_Bitmap, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotatedBitmap = rotateImage(pic_Bitmap, 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotatedBitmap = rotateImage(pic_Bitmap, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        rotatedBitmap = pic_Bitmap;
                }

               return rotatedBitmap;

            }
        } catch (Exception e) {
            Log.i("Exception","showBitmapFromFile");
            return null;
        }
        return null;
    }

    public static Bitmap decodeFile(String path) {
        Bitmap b = null;
        File f = new File(path);
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();

            int IMAGE_MAX_SIZE = 1024; // maximum dimension limit
            int scale = 1;
            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;

            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return b;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }*/

}
