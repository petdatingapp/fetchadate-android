package com.example.sudio.fetchadate.Activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.ImageView;

import com.example.sudio.fetchadate.R;

public class SplashAnimActivity extends AppCompatActivity {

    ImageView PawImage1,PawImage2,PawImage3,PawImage4,PawImage5,PawImage6;
    ImageView PawImage7,PawImage8,PawImage9,PawImage10;
    AnimationSet imgAnimSet1;
    AnimationSet imgAnimSet2;
    AnimationSet imgAnimSet3;
    AnimationSet imgAnimSet4;
    AnimationSet imgAnimSet5;
    AnimationSet imgAnimSet6;
    AnimationSet imgAnimSet7;
    AnimationSet imgAnimSet8;
    AnimationSet imgAnimSet9;
    AnimationSet imgAnimSet10;

    AlphaAnimation fadeIn;
    AlphaAnimation fadeOut;

    public int fadeInOut;
    public int fadeInOutDuration;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sp_anim);
        PawImage1 = (ImageView) findViewById(R.id.pawLogo1);
        PawImage2 = (ImageView) findViewById(R.id.pawLogo2);
        PawImage3 = (ImageView) findViewById(R.id.pawLogo3);
        PawImage4 = (ImageView) findViewById(R.id.pawLogo4);
        PawImage5 = (ImageView) findViewById(R.id.pawLogo5);
        PawImage6 = (ImageView) findViewById(R.id.pawLogo6);
        PawImage7 = (ImageView) findViewById(R.id.pawLogo7);
        PawImage8 = (ImageView) findViewById(R.id.pawLogo8);
        PawImage9 = (ImageView) findViewById(R.id.pawLogo9);
        PawImage10 = (ImageView) findViewById(R.id.pawLogo10);

        PawImage1.setVisibility(View.INVISIBLE);
        PawImage2.setVisibility(View.INVISIBLE);
        PawImage3.setVisibility(View.INVISIBLE);
        PawImage4.setVisibility(View.INVISIBLE);
        PawImage5.setVisibility(View.INVISIBLE);
        PawImage6.setVisibility(View.INVISIBLE);
        PawImage7.setVisibility(View.INVISIBLE);
        PawImage8.setVisibility(View.INVISIBLE);
        PawImage9.setVisibility(View.INVISIBLE);
        PawImage10.setVisibility(View.INVISIBLE);

        fadeInOutDuration=300;
        fadeIn=new AlphaAnimation(0,1);
        fadeOut=new AlphaAnimation(1,0);

        imgAnimSet1 = new AnimationSet(false);
        imgAnimSet1.addAnimation(fadeIn);
        imgAnimSet1.setDuration(fadeInOutDuration);

        imgAnimSet2 = new AnimationSet(false);
        imgAnimSet2.addAnimation(fadeIn);
        imgAnimSet2.setDuration(fadeInOutDuration);

        imgAnimSet3 = new AnimationSet(false);
        imgAnimSet3.addAnimation(fadeIn);
        imgAnimSet3.setDuration(fadeInOutDuration);

        imgAnimSet4 = new AnimationSet(false);
        imgAnimSet4.addAnimation(fadeIn);
        imgAnimSet4.setDuration(fadeInOutDuration);

        imgAnimSet5 = new AnimationSet(false);
        imgAnimSet5.addAnimation(fadeIn);
        imgAnimSet5.setDuration(fadeInOutDuration);

        imgAnimSet6 = new AnimationSet(false);
        imgAnimSet6.addAnimation(fadeIn);
        imgAnimSet6.setDuration(fadeInOutDuration);

        imgAnimSet7 = new AnimationSet(false);
        imgAnimSet7.addAnimation(fadeIn);
        imgAnimSet7.setDuration(fadeInOutDuration);

        imgAnimSet8 = new AnimationSet(false);
        imgAnimSet8.addAnimation(fadeIn);
        imgAnimSet8.setDuration(fadeInOutDuration);

        imgAnimSet9 = new AnimationSet(false);
        imgAnimSet9.addAnimation(fadeIn);
        imgAnimSet9.setDuration(fadeInOutDuration);

        imgAnimSet10 = new AnimationSet(false);
        imgAnimSet10.addAnimation(fadeIn);
        imgAnimSet10.setDuration(fadeInOutDuration);

        PawImage10.setVisibility(View.VISIBLE);
        PawImage10.startAnimation(imgAnimSet1);
        fadeInOut=1;

        imgAnimSet1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                if(fadeInOut==1) {
                    PawImage10.clearAnimation();
                    PawImage9.setVisibility(View.VISIBLE);
                    PawImage9.startAnimation(imgAnimSet2);
                }
                else{
                    PawImage10.clearAnimation();
                    PawImage10.setVisibility(View.INVISIBLE);
                    imgAnimSet2.reset();
                    imgAnimSet2.addAnimation(fadeOut);
                    imgAnimSet2.setDuration(fadeInOutDuration);
                    PawImage9.startAnimation(imgAnimSet2);
                }
            }
        });


        imgAnimSet2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                if(fadeInOut==1) {
                PawImage9.clearAnimation();
                PawImage8.setVisibility(View.VISIBLE);
                PawImage8.startAnimation(imgAnimSet3);
            }

                else {
                    PawImage9.clearAnimation();
                    PawImage9.setVisibility(View.INVISIBLE);
                    imgAnimSet3.reset();
                    imgAnimSet3.addAnimation(fadeOut);
                    imgAnimSet3.setDuration(fadeInOutDuration);
                    PawImage8.startAnimation(imgAnimSet3);

                }
            }
        });


        imgAnimSet3.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                if(fadeInOut==1) {
                PawImage8.clearAnimation();
                PawImage7.setVisibility(View.VISIBLE);
                PawImage7.startAnimation(imgAnimSet4);
                }

                else {
                    PawImage8.clearAnimation();
                    PawImage8.setVisibility(View.INVISIBLE);
                    imgAnimSet4.reset();
                    imgAnimSet4.addAnimation(fadeOut);
                    imgAnimSet4.setDuration(fadeInOutDuration);
                    PawImage7.startAnimation(imgAnimSet4);
                }
            }
        });

        imgAnimSet4.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                if(fadeInOut==1) {
                PawImage7.clearAnimation();
                PawImage6.setVisibility(View.VISIBLE);
                PawImage6.startAnimation(imgAnimSet5);
                }

                else {
                    PawImage7.clearAnimation();
                    PawImage7.setVisibility(View.INVISIBLE);
                    imgAnimSet5.reset();
                    imgAnimSet5.addAnimation(fadeOut);
                    imgAnimSet5.setDuration(fadeInOutDuration);
                    PawImage6.startAnimation(imgAnimSet5);
                }
            }
        });

        imgAnimSet5.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                if(fadeInOut==1) {
                PawImage6.clearAnimation();
                PawImage5.setVisibility(View.VISIBLE);
                PawImage5.startAnimation(imgAnimSet6);
                }

                else {
                    PawImage6.clearAnimation();
                    PawImage6.setVisibility(View.INVISIBLE);
                    imgAnimSet6.reset();
                    imgAnimSet6.addAnimation(fadeOut);
                    imgAnimSet6.setDuration(fadeInOutDuration);
                    PawImage5.startAnimation(imgAnimSet6);
                }
            }
        });


        imgAnimSet6.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                if(fadeInOut==1) {
                PawImage5.clearAnimation();
                PawImage4.setVisibility(View.VISIBLE);
                PawImage4.startAnimation(imgAnimSet7);
                }

                else {
                    PawImage5.clearAnimation();
                    PawImage5.setVisibility(View.INVISIBLE);
                    imgAnimSet7.reset();
                    imgAnimSet7.addAnimation(fadeOut);
                    imgAnimSet7.setDuration(fadeInOutDuration);
                    PawImage4.startAnimation(imgAnimSet7);
                }
            }
        });

        imgAnimSet7.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                if(fadeInOut==1) {
                PawImage4.clearAnimation();
                PawImage3.setVisibility(View.VISIBLE);
                PawImage3.startAnimation(imgAnimSet8);
                }

                else {
                    PawImage4.clearAnimation();
                    PawImage4.setVisibility(View.INVISIBLE);
                    imgAnimSet8.reset();
                    imgAnimSet8.addAnimation(fadeOut);
                    imgAnimSet8.setDuration(fadeInOutDuration);
                    PawImage3.startAnimation(imgAnimSet8);
                }
            }
        });

        imgAnimSet8.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                if(fadeInOut==1) {
                PawImage3.clearAnimation();
                PawImage2.setVisibility(View.VISIBLE);
                PawImage2.startAnimation(imgAnimSet9);
                }

                else {
                    PawImage3.clearAnimation();
                    PawImage3.setVisibility(View.INVISIBLE);
                    imgAnimSet9.reset();
                    imgAnimSet9.addAnimation(fadeOut);
                    imgAnimSet9.setDuration(fadeInOutDuration);
                    PawImage2.startAnimation(imgAnimSet9);
                    }
            }
        });


        imgAnimSet9.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                if(fadeInOut==1) {
                    PawImage2.clearAnimation();
                    PawImage1.setVisibility(View.VISIBLE);
                    PawImage1.startAnimation(imgAnimSet10);
                }

                else {
                    PawImage2.clearAnimation();
                    PawImage2.setVisibility(View.INVISIBLE);
                    imgAnimSet10.reset();
                    imgAnimSet10.addAnimation(fadeOut);
                    imgAnimSet10.setDuration(fadeInOutDuration);
                    PawImage1.startAnimation(imgAnimSet10);
                    fadeInOut=0;
                }
            }
        });
        imgAnimSet10.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
//                if(fadeInOut==0) {
//                    PawImage1.clearAnimation();
//                    PawImage1.setVisibility(View.GONE);
//                    Intent i =new Intent(SplashAnimActivity.this,SignInActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(i);
//                    finish();
//                }
                if(fadeInOut==1) {
                PawImage1.clearAnimation();
              //  fadeInOut=2;
                    Intent i =new Intent(SplashAnimActivity.this,SignInActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }
                if(fadeInOut==2) {
                    imgAnimSet1.reset();
                    imgAnimSet1.addAnimation(fadeOut);
                    PawImage10.startAnimation(imgAnimSet1);
                }

            }
        });


    }


}
