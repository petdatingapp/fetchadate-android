package com.example.sudio.fetchadate.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sudio.fetchadate.Custom.CircleImageView;
import com.example.sudio.fetchadate.R;

import java.util.ArrayList;
import java.util.List;

public class MyFavsMsgItemsAdapter extends RecyclerView.Adapter<MyFavsMsgItemsAdapter.ViewHolder> {

    private List<String> mDataLoversNames;

    private ArrayList<Integer> mAminalImg ;
    private ArrayList<Integer> mAminalLovers ;

    private LayoutInflater mInflater;
    private MyFasMsgItemClickListener mMyFavMsgClickListener;
    private Context mContext;

    // dataDogs is passed into the constructor
    public MyFavsMsgItemsAdapter(Context context, List<String> dataDogs, List<String> dataLovers, ArrayList<Integer> AminalImages, ArrayList<Integer> AminalLovers) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mDataLoversNames = dataLovers;
        this.mAminalImg = AminalImages;
        this.mAminalLovers = AminalLovers;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_favs_messages, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String dogLoversName = mDataLoversNames.get(position);

        holder.itemFavsMsg_txtLoverName.setText(dogLoversName);
        holder.itemFavsMsg_imgCircleLover.setImageResource(mAminalLovers.get(position));
        holder.itemFavsMsg_ImgLover.setImageResource(mAminalLovers.get(position));
        holder.itemFavsMsg_ImgCircleDog.setImageResource(mAminalImg.get(position));
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mDataLoversNames.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout itemMyFavMsg_LinMain;
        ImageView itemFavsMsg_ImgMessage,itemFavsMsg_ImgLover,itemFavsMsg_Menu;
        TextView itemFavsMsg_txtLoverName;
        TextView itemFavsMsg_txtProfession,itemFavsMsg_txtUniversity,itemFavsMsg_txtDistance;
        CircleImageView itemFavsMsg_imgCircleLover,itemFavsMsg_ImgCircleDog;

        ViewHolder(View itemView) {
            super(itemView);
            itemMyFavMsg_LinMain = (LinearLayout) itemView.findViewById(R.id.itemMyFavMsg_LinMain);
            itemFavsMsg_ImgMessage = (ImageView) itemView.findViewById(R.id.itemFavsMsg_ImgMessage);
            itemFavsMsg_ImgLover = (ImageView) itemView.findViewById(R.id.itemFavsMsg_ImgLover);
            itemFavsMsg_Menu = (ImageView) itemView.findViewById(R.id.itemFavsMsg_Menu);

            itemFavsMsg_imgCircleLover = (CircleImageView) itemView.findViewById(R.id.itemFavsMsg_imgCircleLover);
            itemFavsMsg_ImgCircleDog = (CircleImageView) itemView.findViewById(R.id.itemFavsMsg_ImgCircleDog);

            itemFavsMsg_txtLoverName = (TextView) itemView.findViewById(R.id.itemFavsMsg_txtLoverName);
            itemFavsMsg_txtProfession = (TextView) itemView.findViewById(R.id.itemFavsMsg_txtProfession);
            itemFavsMsg_txtUniversity = (TextView) itemView.findViewById(R.id.itemFavsMsg_txtUniversity);
            itemFavsMsg_txtDistance = (TextView) itemView.findViewById(R.id.itemFavsMsg_txtDis);

            itemFavsMsg_Menu.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mMyFavMsgClickListener != null) mMyFavMsgClickListener.onMyFavMsgItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return mDataLoversNames.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(MyFasMsgItemClickListener itemClickListener) {
        this.mMyFavMsgClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface MyFasMsgItemClickListener {
        void onMyFavMsgItemClick(View view, int position);
    }
}