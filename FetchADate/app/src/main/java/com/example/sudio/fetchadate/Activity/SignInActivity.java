package com.example.sudio.fetchadate.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sudio.fetchadate.R;


public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView loginImgFaceook,imgInstagram,imgGooglePlus;
    Intent iGuyGalInt;

    private final String TAG = SignInActivity.this.getClass().getName();

//    private CallbackManager callbackManager;
//    private AccessToken accessToken;

    //private LoginButton loginImgFaceook;
    private RelativeLayout rlProfileArea;
    private TextView tvName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        loginImgFaceook = (ImageView)findViewById(R.id.login_FbLogo);
        imgInstagram = (ImageView)findViewById(R.id.login_InstagramLogo);
        imgGooglePlus = (ImageView)findViewById(R.id.login_GplusLogo);
        loginImgFaceook.setOnClickListener(this);
        imgInstagram.setOnClickListener(this);
        imgGooglePlus.setOnClickListener(this);

//        initParameters();
//        initViews();


//        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
//            @Override
//            protected void onCurrentAccessTokenChanged(
//                    AccessToken oldAccessToken,
//                    AccessToken currentAccessToken) {
//
//                if (currentAccessToken == null) {
//                    Log.d(TAG,"User logged out successfully");
//                   // rlProfileArea.setVisibility(View.GONE);
//                }
//            }
//        };
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            //handle multiple view click events
            case R.id.login_FbLogo:
                iGuyGalInt = new Intent(SignInActivity.this,GuyGalActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(iGuyGalInt);
                break;
            case R.id.login_InstagramLogo:
                iGuyGalInt = new Intent(SignInActivity.this,GuyGalActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(iGuyGalInt);
                break;
            case R.id.login_GplusLogo:
                iGuyGalInt = new Intent(SignInActivity.this,GuyGalActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(iGuyGalInt);
                break;
        }
    }


//    public void initParameters() {
//        accessToken = AccessToken.getCurrentAccessToken();
//        callbackManager = CallbackManager.Factory.create();
//
//    }

//    public void initViews() {
//
////        rlProfileArea = (RelativeLayout) findViewById(R.id.activity_main_rl_profile_area);
//        tvName = (TextView) findViewById(R.id.txtLoginStatus);
//
//        loginImgFaceook.setReadPermissions(Arrays.asList(new String[]{"email", "user_birthday", "user_hometown"}));
//        if (accessToken != null) {
//            getProfileData();
//        } else {
//            tvName.setVisibility(View.GONE);
//        }
//
//// Callback registration
//        loginImgFaceook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Log.d(TAG, "User login successfully");
//                getProfileData();
//            }
//
//            @Override
//            public void onCancel() {
//                // App code
//                Log.d(TAG, "User cancel login");
//            }
//
//            @Override
//            public void onError(FacebookException exception) {
//                // App code
//                Log.d(TAG, "Problem for login");
//            }
//        });
//
//
//    }
//
//
//    public void getProfileData() {
//        try {
//            accessToken = AccessToken.getCurrentAccessToken();
//            tvName.setVisibility(View.VISIBLE);
//            GraphRequest request = GraphRequest.newMeRequest(
//                    accessToken,
//                    new GraphRequest.GraphJSONObjectCallback() {
//                        @Override
//                        public void onCompleted(
//                                JSONObject object,
//                                GraphResponse response) {
//                            Log.d(TAG, "Graph Object :" + object);
//                            try {
//                                String name = object.getString("name");
//                                tvName.setText("Welcome, " + name);
//
//                                Log.d(TAG, "Name :" + name);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//            Bundle parameters = new Bundle();
//            parameters.putString("fields", "id,name,link,birthday,gender,email");
//            request.setParameters(parameters);
//            request.executeAsync();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode, resultCode, data);
//    }

}
