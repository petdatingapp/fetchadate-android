package com.example.sudio.fetchadate.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sudio.fetchadate.R;

public class ProfileFragment extends Fragment implements View.OnClickListener{

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        init();
        click();

        return rootView;
    }

    private void init(){

    }

    private void click(){

    }

    @Override
    public void onClick(View v) {

    }
}
