package com.example.sudio.fetchadate.Activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sudio.fetchadate.Fragment.DogLoversFragment;
import com.example.sudio.fetchadate.Fragment.FavdMeFragment;
import com.example.sudio.fetchadate.Fragment.FavsMessagesFragment;
import com.example.sudio.fetchadate.Fragment.MessagesFragment;
import com.example.sudio.fetchadate.Fragment.MyFavsFragment;
import com.example.sudio.fetchadate.Fragment.ProfileFragment;
import com.example.sudio.fetchadate.Fragment.WhosBehindFragment;
import com.example.sudio.fetchadate.R;


public class MainActivity extends AppCompatActivity implements View.OnClickListener,MyFavsFragment.MyFavInterface {

    public LinearLayout llMessages, llFavs, llDogLovers, llWhosBehind, llProfile;
    private ImageView ivLeftMenu, ivRightMenu, ivMessages, ivFavs, ivDogLovers, ivWhosBehind, ivProfile;
    private TextView tvMessages, tvFavs, tvDogLovers, tvWhosBehind, tvProfile;
    private Fragment fragmentCurrent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            getSupportActionBar().hide();
            setContentView(R.layout.activity_main);
        }catch (Exception ex){
        ex.getMessage();
        }
        init();
        click();
    }

    private void init(){
        llMessages = (LinearLayout) findViewById(R.id.llMessages);
        llFavs = (LinearLayout) findViewById(R.id.llFavs);
        llDogLovers = (LinearLayout) findViewById(R.id.llDogLovers);
        llWhosBehind = (LinearLayout) findViewById(R.id.llWhosBehind);
        llProfile = (LinearLayout) findViewById(R.id.llProfile);
        ivLeftMenu = (ImageView) findViewById(R.id.ivLeftMenu);
        ivRightMenu = (ImageView) findViewById(R.id.ivRightMenu);
        ivMessages = (ImageView) findViewById(R.id.ivMessages);
        ivFavs = (ImageView) findViewById(R.id.ivFavs);
        ivDogLovers = (ImageView) findViewById(R.id.ivDogLovers);
        ivWhosBehind = (ImageView) findViewById(R.id.ivWhosBehind);
        ivProfile = (ImageView) findViewById(R.id.ivProfile);
        tvMessages = (TextView) findViewById(R.id.tvMessages);
        tvFavs = (TextView) findViewById(R.id.tvFavs);
        tvDogLovers = (TextView) findViewById(R.id.tvDogLovers);
        tvWhosBehind = (TextView) findViewById(R.id.tvWhosBehind);
        tvProfile = (TextView) findViewById(R.id.tvProfile);

        loadMessagesFragment();
    }

    private void click(){
        llMessages.setOnClickListener(MainActivity.this);
        llFavs.setOnClickListener(MainActivity.this);
        llDogLovers.setOnClickListener(MainActivity.this);
        llWhosBehind.setOnClickListener(MainActivity.this);
        llProfile.setOnClickListener(MainActivity.this);
        ivRightMenu.setOnClickListener(MainActivity.this);
    }

    @Override
    public void onClick(View v) {

        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.frame_container);
        if(v == llMessages && currentFragment instanceof MessagesFragment == false){
            loadMessagesFragment();
        } else if(v == llFavs && currentFragment instanceof MyFavsFragment == false){
            loadMyFavsFragment();
        } else if(v == llDogLovers && currentFragment instanceof DogLoversFragment == false){
            loadDogLoversFragment();
        } else if(v == llWhosBehind && currentFragment instanceof WhosBehindFragment == false){
            loadWhosBehindFragment();
        } else if(v == llProfile && currentFragment instanceof ProfileFragment == false){
            loadProfileFragment();
        }

//        else if(v == ivRightMenu && currentFragment instanceof MyFavsFragment == true){
//            try{
//                ((MyFavsFragment) fragmentCurrent).showMyFavMenu();
//              //  myFavlistener.myFavAction();
//            }catch (Exception ex){
//
//            }
//        }
//        else if(v == ivRightMenu && currentFragment instanceof FavsMessagesFragment == true){
//            try{
//                ((FavsMessagesFragment) fragmentCurrent).showMyFavMsgMenu();
//                //  myFavlistener.myFavAction();
//            }catch (Exception ex){
//
//            }
//        }
    }



    public void loadMessagesFragment(){
        resetTab();
        ivMessages.setImageResource(R.drawable.messages_red);
        tvMessages.setTextColor(getResources().getColor(R.color.colorTabSelected));

        Fragment fragment = new MessagesFragment();
        fragmentCurrent=fragment;
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment,"Tag_MyMsgFrag");
        fragmentTransaction.commit();
    }

    public void loadFavsMessagesFragment(){
        resetTab();
        ivFavs.setImageResource(R.drawable.favs_red);
        tvFavs.setTextColor(getResources().getColor(R.color.colorTabSelected));
//        ivRightMenu.setVisibility(View.VISIBLE);
//        ivRightMenu.setImageResource(R.drawable.ic_more_vert_black_24dp);

        Fragment fragment = new FavsMessagesFragment();
        fragmentCurrent=fragment;
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment,"Tag_FavMsgFrag");
        fragmentTransaction.commit();
    }

    public void loadMyFavsFragment(){
        resetTab();
        ivFavs.setImageResource(R.drawable.favs_red);
        tvFavs.setTextColor(getResources().getColor(R.color.colorTabSelected));
//        ivRightMenu.setVisibility(View.VISIBLE);
//        ivRightMenu.setImageResource(R.drawable.ic_more_vert_black_24dp);

        Fragment fragment = new MyFavsFragment();
        fragmentCurrent=fragment;
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment,"Tag_MyFavMsgFrag");

        fragmentTransaction.commit();
    }

    public void loadFavdMeFragment(){
        resetTab();
        ivFavs.setImageResource(R.drawable.favs_red);
        tvFavs.setTextColor(getResources().getColor(R.color.colorTabSelected));

        Fragment fragment = new FavdMeFragment();
        fragmentCurrent=fragment;
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment,"Tag_FavMeFrag");
        fragmentTransaction.commit();
    }

    public void loadDogLoversFragment(){
        resetTab();
        ivLeftMenu.setVisibility(View.VISIBLE);
        ivRightMenu.setVisibility(View.VISIBLE);
        ivLeftMenu.setImageResource(R.drawable.back );
        ivRightMenu.setImageResource(R.drawable.menu );

        ivDogLovers.setImageResource(R.drawable.dog_lovers_red);
        tvDogLovers.setTextColor(getResources().getColor(R.color.colorTabSelected));

        Fragment fragment = new DogLoversFragment();
        fragmentCurrent=fragment;
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment,"Tag_DogLoversFrag");
        fragmentTransaction.commit();
    }

    public void loadWhosBehindFragment(){
        resetTab();
        ivLeftMenu.setVisibility(View.VISIBLE);
        ivRightMenu.setVisibility(View.VISIBLE);
        ivLeftMenu.setImageResource(R.drawable.back );
        ivRightMenu.setImageResource(R.drawable.menu );

        ivWhosBehind.setImageResource(R.drawable.whos_behind_red);
        tvWhosBehind.setTextColor(getResources().getColor(R.color.colorTabSelected));

        Fragment fragment = new WhosBehindFragment();
        fragmentCurrent=fragment;
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment,"Tag_WhoseBehindFrag");
        fragmentTransaction.commit();
    }

    private void loadProfileFragment(){
        resetTab();
        ivLeftMenu.setVisibility(View.VISIBLE);
        ivRightMenu.setVisibility(View.VISIBLE);
        ivLeftMenu.setImageResource(R.drawable.left_setting );
        ivRightMenu.setImageResource(R.drawable.right_profile );
        ivProfile.setImageResource(R.drawable.profile_red);
        tvProfile.setTextColor(getResources().getColor(R.color.colorTabSelected));

        Fragment fragment = new ProfileFragment();
        fragmentCurrent=fragment;
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment,"Tag_ProfileFrag");
        fragmentTransaction.commit();
    }

    private void resetTab(){

        getSupportActionBar().hide();
        ivLeftMenu.setVisibility(View.GONE);
        ivRightMenu.setVisibility(View.GONE);

        ivMessages.setImageResource(R.drawable.messages);
        ivFavs.setImageResource(R.drawable.favs);
        ivDogLovers.setImageResource(R.drawable.dog_lovers);
        ivWhosBehind.setImageResource(R.drawable.whos_behind);
        ivProfile.setImageResource(R.drawable.profile);

        tvMessages.setTextColor(getResources().getColor(R.color.colorTabNotSelected));
        tvFavs.setTextColor(getResources().getColor(R.color.colorTabNotSelected));
        tvDogLovers.setTextColor(getResources().getColor(R.color.colorTabNotSelected));
        tvWhosBehind.setTextColor(getResources().getColor(R.color.colorTabNotSelected));
        tvProfile.setTextColor(getResources().getColor(R.color.colorTabNotSelected));
    }

/*******************************************************************/
/*************************** Fragment Actions *********************/
/******************************************************************/

    @Override
    public void myFavAction() {
        try{
       //     Toast.makeText(getApplicationContext(), "Main Activity of Fragment", Toast.LENGTH_LONG).show();
        }catch (Exception ex){
            ex.getMessage();
        }
    }
}
