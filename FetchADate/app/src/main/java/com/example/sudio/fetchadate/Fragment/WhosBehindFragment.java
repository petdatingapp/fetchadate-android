package com.example.sudio.fetchadate.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.LinearLayout;

import com.example.sudio.fetchadate.R;

public class WhosBehindFragment extends Fragment implements View.OnClickListener{

    private View rootView;
    private LinearLayout llDouleTap, llMoreInfo;

    AnimationSet imgAnimSet1;
    AnimationSet imgAnimSet2;

    AlphaAnimation fadeIn;
    AlphaAnimation fadeOut;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_whos_behind, container, false);

        init();
        click();

        return rootView;
    }

    private void init(){
        llDouleTap = rootView.findViewById(R.id.llDouleTap);
        llMoreInfo = rootView.findViewById(R.id.llMoreInfo);

        fadeIn = new AlphaAnimation(0,1);
        fadeOut = new AlphaAnimation(1,0);

        imgAnimSet1 = new AnimationSet(false);
        imgAnimSet1.addAnimation(fadeIn);
        imgAnimSet1.setDuration(1000);

        imgAnimSet2 = new AnimationSet(false);
        imgAnimSet2.addAnimation(fadeIn);
        imgAnimSet2.setDuration(1000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                llDouleTap.setVisibility(View.VISIBLE);
                llDouleTap.startAnimation(imgAnimSet1);
            }
        }, 3000);


        imgAnimSet1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        llDouleTap.setAnimation(fadeOut);
                        llDouleTap.setVisibility(View.GONE);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                llMoreInfo.setVisibility(View.VISIBLE);
                                llMoreInfo.startAnimation(imgAnimSet2);
                            }
                        }, 2000);
                    }
                }, 2000);
            }
        });

        imgAnimSet2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        llMoreInfo.clearAnimation();
                        llMoreInfo.setVisibility(View.GONE);
                    }
                }, 2000);
            }
        });
    }

    private void click(){

    }

    @Override
    public void onClick(View v) {

    }
}
