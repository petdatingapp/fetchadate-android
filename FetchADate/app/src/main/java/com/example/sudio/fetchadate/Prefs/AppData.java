package com.example.sudio.fetchadate.Prefs;



import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.example.sudio.fetchadate.R;

public class AppData {
	

	
//	public static DbHelper DBHelper;
	
	public static boolean FromSettingsScreen=false;/**To check first time setup or Settings**/
	
	public static String Fb_Profile_Picture;

//	public static Facebook mFacebook;
//	public static AsyncFacebookRunner mAsyncRunner;
    

//	public static String FBuseremail;
//	public static String FBProfile_id="";
//	public static String TwitterProfile_id;
	
	public static String LoginWith;
	
//	public static String KEY_HOME_TO_DETAILS="homeToDetails";
	
//	public static boolean isFbToBeRefreshed=false;
	
	public static boolean isFirstime=false;
	public static boolean isGplusPost=false;

	public static boolean isAccountSelected=false;

    
	public static boolean isOnline(Context c) {
		ConnectivityManager cm = (ConnectivityManager)c.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			// test for connection for WIFI
			if (info != null
			&& info.isAvailable()
			&& info.isConnected()) {
			return true;
			}

			info = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			// test for connection for Mobile
		return info != null
				&& info.isAvailable()
				&& info.isConnected();

	}
	
	public static void ShowToast(Context context)
	{
		Toast.makeText(context, context.getResources().getString(R.string.txt_internet_checking), Toast.LENGTH_SHORT).show();
	}
	public static void ShowToast(Context context,String msg)
	{
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}



	public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public static boolean checkPermission(final Context context)
	{
		int currentAPIVersion = Build.VERSION.SDK_INT;
		if(currentAPIVersion>=android.os.Build.VERSION_CODES.M)
		{
			if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
				if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
					AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
					alertBuilder.setCancelable(true);
					alertBuilder.setTitle("Permission necessary");
					alertBuilder.setMessage("External storage permission is necessary");
					alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
						public void onClick(DialogInterface dialog, int which) {
							ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
						}
					});
					AlertDialog alert = alertBuilder.create();
					alert.show();
				} else {
					ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
				}
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	public static void hideKeyboard(Context c, View view) {
		InputMethodManager inputMethodManager =(InputMethodManager)c.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	
}
