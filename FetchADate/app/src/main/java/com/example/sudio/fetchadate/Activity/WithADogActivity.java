package com.example.sudio.fetchadate.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.sudio.fetchadate.R;

public class WithADogActivity extends AppCompatActivity implements View.OnClickListener{

    ProgressBar iProgrssBar;
    ImageButton imgBtnBack;
    Button WithDogWODog_btnWDog, WithDogWODog_btnWoutDog, btnNext;
    int iFlagWithDog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wd_wod);

        iProgrssBar = (ProgressBar)findViewById(R.id.progressBar_DogLover);
        iProgrssBar.setProgress(40);

        imgBtnBack = (ImageButton)findViewById(R.id.ImgButtonBack_DogLover);
        WithDogWODog_btnWDog = (Button)findViewById(R.id.WithDogWODog_btnWDog);
        WithDogWODog_btnWoutDog = (Button)findViewById(R.id.WithDogWODog_btnWoutDog);
        btnNext = (Button)findViewById(R.id.WithDogWODog_btnNext);

        imgBtnBack.setOnClickListener(this);
        WithDogWODog_btnWDog.setOnClickListener(this);
        WithDogWODog_btnWoutDog.setOnClickListener(this);
        btnNext.setOnClickListener(this);

        iFlagWithDog=0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(iFlagWithDog>0) {
            setValueWADogWoAdog();
        }
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            //handle multiple view click events
            case R.id.ImgButtonBack_DogLover:
                Intent iSigninInt = new Intent(WithADogActivity.this,GuyGalActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(iSigninInt);
                break;

            case R.id.WithDogWODog_btnWDog:
                iFlagWithDog=1;
                setValueWADogWoAdog();
                break;

            case R.id.WithDogWODog_btnWoutDog:
                iFlagWithDog=2;
                setValueWADogWoAdog();
                break;

            case R.id.WithDogWODog_btnNext:
                if(iFlagWithDog==0){
                    Toast.makeText(getApplicationContext(), R.string.WithDogWODog_ValidationMessage, Toast.LENGTH_LONG).show();
                }else {
                    Intent iWithADogint = new Intent(WithADogActivity.this, DogNameActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(iWithADogint);
                }
                break;

        }
    }

    public void setValueWADogWoAdog(){
        if(iFlagWithDog==1){
            ((Button) findViewById(R.id.WithDogWODog_btnWDog)).setTextColor(getResources().getColor(R.color.colorAccent));
            ((Button) findViewById(R.id.WithDogWODog_btnWDog)).setBackgroundResource(R.drawable.mycusto_btn2);
            ((Button) findViewById(R.id.WithDogWODog_btnWoutDog)).setTextColor(getResources().getColor(R.color.colorBlack));
            ((Button) findViewById(R.id.WithDogWODog_btnWoutDog)).setBackgroundResource(R.drawable.mycusto_btn1);
            ((Button) findViewById(R.id.WithDogWODog_btnNext)).setBackground(getResources().getDrawable(R.drawable.button_normal));
            }
            else if(iFlagWithDog==2){
            ((Button) findViewById(R.id.WithDogWODog_btnWoutDog)).setTextColor(getResources().getColor(R.color.colorAccent));
            ((Button) findViewById(R.id.WithDogWODog_btnWoutDog)).setBackgroundResource(R.drawable.mycusto_btn2);
            ((Button) findViewById(R.id.WithDogWODog_btnWDog)).setTextColor(getResources().getColor(R.color.colorBlack));
            ((Button) findViewById(R.id.WithDogWODog_btnWDog)).setBackgroundResource(R.drawable.mycusto_btn1);
            ((Button) findViewById(R.id.WithDogWODog_btnNext)).setBackground(getResources().getDrawable(R.drawable.button_normal));
        }
    }
}
