package com.example.sudio.fetchadate.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.sudio.fetchadate.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DogDobActivity extends AppCompatActivity implements View.OnClickListener{

    ImageButton imgBtnBack;
    ProgressBar iProgrssBar;
    Button btnNext;

    EditText edtDobdog;
    Calendar myCalendar;
    //DatePickerDialog.OnDateSetListener date;
    DatePickerDialog mDatePicker;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dog_dob);
        imgBtnBack = (ImageButton)findViewById(R.id.ImgButtonBack_DogLover);
        imgBtnBack.setOnClickListener(this);
        iProgrssBar = (ProgressBar)findViewById(R.id.progressBar_DogLover);
        iProgrssBar.setProgress(80);

        btnNext = (Button)findViewById(R.id.DogDob_btnNext);
        btnNext.setOnClickListener(this);

        edtDobdog=(EditText)findViewById(R.id.DogDob_edtDobdog);
        edtDobdog.setOnClickListener(this);

        myCalendar = Calendar.getInstance();

        edtDobdog.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                View DV = (View)findViewById(R.id.DogDob_DividerView);
                try {
                    if (editable.toString().length()>0) {
                        DV.setBackgroundResource(R.color.colorAccent);
                    }
                    else if (editable.toString().length()==0) {
                        DV.setBackgroundResource(R.color.colorLightGrey);
                    }
                }catch (Exception e){

                }

            }
        });

    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            //handle multiple view click events
            case R.id.ImgButtonBack_DogLover:
                Intent iIntent = new Intent(DogDobActivity.this,DogNameActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(iIntent);
                break;

            case R.id.DogDob_btnNext:
                if(edtDobdog.getText().toString().equals("")){
                    edtDobdog.setError(this.getString(R.string.DogDob_ValidationMessage));
                    //Toast.makeText(DogDobActivity.this,this.getString(R.string.DogDob_ValidationMessage),Toast.LENGTH_LONG);
                }
                else {
                    Intent iWithADogint = new Intent(DogDobActivity.this, DogPhotoActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(iWithADogint);
                }
                break;

            case R.id.DogDob_edtDobdog:

                showDatePicker();
                break;

        }
    }


    private void showDatePicker()
    {
     mDatePicker = new DatePickerDialog(DogDobActivity.this, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth)
            {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                if(getAge(year, monthOfYear,dayOfMonth)==false){
               // Toast.makeText(DogDobActivity.this,getString(R.string.DogDob_LimitValMsg),Toast.LENGTH_LONG);
                Toast toast = Toast.makeText(DogDobActivity.this,getString(R.string.DogDob_LimitValMsg),Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                }else if(getAge(year, monthOfYear,dayOfMonth)==true){
                    updateLabel();
                }
            }
        },myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH));

        //mDatePicker.setTitle("Please select DOB");
        mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePicker.show();
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        edtDobdog.setError(null);
        edtDobdog.clearFocus();
        edtDobdog.setText(sdf.format(myCalendar.getTime()));
    }

    private boolean getAge(int year, int month, int day) {
        try {
            Calendar dob = Calendar.getInstance();
            Calendar today = Calendar.getInstance();
            dob.set(year, month, day);
            int monthToday = today.get(Calendar.MONTH) + 1;
            int monthDOB = dob.get(Calendar.MONTH)+1;
            int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
            if (age > 18) {
                return true;
            } else if (age == 18) {
                if (monthDOB > monthToday) {
                    return true;
                } else if (monthDOB == monthToday) {
                    int todayDate = today.get(Calendar.DAY_OF_MONTH);
                    int dobDate = dob.get(Calendar.DAY_OF_MONTH);
                    if (dobDate <= todayDate) { // should be less then
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
